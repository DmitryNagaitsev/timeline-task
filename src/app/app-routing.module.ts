import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from './views/not-found';

const routes: Routes = [
    { path: '', redirectTo: '/events', pathMatch: 'full'},
    { path: 'events', loadChildren: './views/events/events.module#EventsModule'},
    { path: '**', component: NotFoundComponent },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
