import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEventComponent } from './add-event/add-event.component';
import { EventsRegistryComponent } from './events-registry';
import { EventDisplayComponent } from './event-display';

const routes: Routes = [
    { path: '', component: EventsRegistryComponent},
    { path: 'registry', component: EventsRegistryComponent},
    { path: 'display/:id', component: EventDisplayComponent },
    { path: 'create', component: AddEventComponent}
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class EventsRoutingModule {}
