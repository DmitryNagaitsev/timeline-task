import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Event } from '../models/event';

@Injectable()
export class AddEventService {
  private addEventSource = new Subject<Event>();

  public eventAdded = this.addEventSource.asObservable();

  publishAddEventRequest(event: Event) {
      this.addEventSource.next(event);
  }
}
