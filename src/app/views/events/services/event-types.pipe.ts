import { Pipe, PipeTransform } from '@angular/core';
import { EventTypes } from '../models/event-types';

@Pipe({name: 'eventTypeName'})
export class EventTypeNamePipe implements PipeTransform {
  transform(value: EventTypes): string {
    switch (value) {
      case EventTypes.News:
        return 'Новость';
      case EventTypes.FinancialTransaction:
        return 'Финансовая транзакция';
      default:
        throw new Error('Неизвестный тип события');
    }
  }
}
