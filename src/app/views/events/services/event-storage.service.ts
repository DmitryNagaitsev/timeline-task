import { Injectable } from '@angular/core';
import * as _ from 'underscore';
import { Event } from '../models/event';
import { EventTypes } from '../models/event-types';
import { News } from '../models/news';

/**
 * TODO: заменить на нормальную бд
 */
@Injectable()
export class EventStorageService {
  private counter = 1;
  private events: Map<number, Event> = new Map();

  public getEvents(): Event[] {
    return Array.from(this.events.values());
  }

  public addEvent(event: Event): void {
    event.id = this.counter;
    this.events.set(this.counter, event);
    this.counter++;
  }

  public removeEvent(id: number): void {
    this.events.delete(id);
  }

  public getById(id: number): Event {
    return this.events.get(id);
  }

  public markViewed(id: number): void {
    const event = this.getById(id);
    if (!event && event.type !== EventTypes.News) {
      throw new Error('Признак прочтения доступен только для новости');
    }
    (<News>event).viewed = true;
  }
}
