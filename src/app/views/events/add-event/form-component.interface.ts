import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

export interface IFormComponent<T> {
  onSubmit: Observable<T>;
}
