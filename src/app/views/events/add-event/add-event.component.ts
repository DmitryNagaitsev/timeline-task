import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import {
  Component,
  Type,
  Output,
  EventEmitter,
  ViewContainerRef,
  ComponentFactoryResolver,
  OnInit,
  ComponentRef,
  ViewChild
} from '@angular/core';

import { EventTypes } from './../models/event-types';
import { EventTypeNamePipe } from '../services/event-types.pipe';
import { resolveFormComponentType } from './add-events.factory';
import { IFormComponent } from './form-component.interface';
import { AddEventService } from '../services/add-event.service';
import { Event } from '../models/event';
import { EventStorageService } from '../services/event-storage.service';
@Component({
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss'],
  providers: [ EventTypeNamePipe ]
})
export class AddEventComponent {
  public selectedValue: EventTypes | null;
  public EventTypes = EventTypes;
  private componentRef: ComponentRef<any>;
  private subscription: Subscription;

  @ViewChild('container', { read: ViewContainerRef }) _vcr;

  public constructor(
    private readonly router: Router,
    private componentFactoryResolver: ComponentFactoryResolver,
    private readonly eventStorage: EventStorageService
  ) { }

  public get eventTypes(): Array<string> {
    const keys = Object.values(this.EventTypes);
    return keys.slice(keys.length / 2);
  }

  public onSelected(type: EventTypes): void {
    // TODO: переделать на модалки
    if (this.componentRef) {
      this.componentRef.destroy();
    }

    const eventContentComponent = resolveFormComponentType(Number(type));
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(eventContentComponent);
    this.componentRef = this._vcr.createComponent(componentFactory);
    (<IFormComponent<Event>>this.componentRef.instance).onSubmit
      .subscribe(formValue => {
        this.eventStorage.addEvent(formValue);
        this.router.navigate(['/events/registry']);
      });
  }
}
