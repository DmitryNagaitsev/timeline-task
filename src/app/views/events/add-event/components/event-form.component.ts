import { ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';
import { Event } from '../../models/event';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IFormComponent } from '../form-component.interface';

export abstract class EventFormComponent implements IFormComponent<Event> {
  public submitted = false;
  @ViewChild('form') form: NgForm;
  private onSubmitSubject = new Subject<Event>();
  public onSubmit = this.onSubmitSubject.asObservable();

  public onFormSubmit(): void {
    this.submitted = true;
    if (this.form.valid) {
      const model = this.getFormValue();
      model.date = new Date();
      this.onSubmitSubject.next(model);
    }
  }

  protected abstract getFormValue(): Event;
}
