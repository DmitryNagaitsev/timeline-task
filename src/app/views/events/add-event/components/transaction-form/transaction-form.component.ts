import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { FinancialTransaction } from './../../../models/financial-transaction';
import { IFormComponent } from '../../form-component.interface';
import { EventFormComponent } from '../event-form.component';

@Component({
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.scss']
})
export class TransactionFormComponent extends EventFormComponent {
  public model = new FinancialTransaction();

  protected getFormValue(): FinancialTransaction {
    return this.model;
  }
}
