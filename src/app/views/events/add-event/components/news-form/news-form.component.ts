import { Component, ViewChild } from '@angular/core';
import { News } from '../../../models/news';
import { IFormComponent } from '../../form-component.interface';
import { NgForm } from '@angular/forms';
import { EventFormComponent } from '../event-form.component';

@Component({
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.scss']
})
export class NewsFormComponent extends EventFormComponent {
  public model = new News();

  protected getFormValue(): News {
    return this.model;
  }
}
