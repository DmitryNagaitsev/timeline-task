import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsRoutingModule } from './events-routing.module';

import { EventDisplayComponent } from './event-display';
import {
  NewsCardComponent,
  TransactionCardComponent,
  EventsRegistryComponent,
  EventCardComponent
} from './events-registry';
import { AddEventComponent } from './add-event/add-event.component';
import { EventTypeNamePipe } from './services/event-types.pipe';
import { NewsFormComponent } from './add-event/components/news-form/news-form.component';
import { TransactionFormComponent } from './add-event/components/transaction-form/transaction-form.component';
import { FormsModule } from '@angular/forms';
import { AddEventService } from './services/add-event.service';
import { NewsFullComponent, TransactionFullComponent } from './event-display/components/cards';

const Event_Card_Components = [
  NewsCardComponent,
  TransactionCardComponent
];

const Event_Form_Components = [
  NewsFormComponent,
  TransactionFormComponent
];

const Event_Full_Components = [
  NewsFullComponent,
  TransactionFullComponent
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        EventsRoutingModule
    ],
    declarations: [
        EventsRegistryComponent,
        EventDisplayComponent,
        ...Event_Full_Components,
        EventCardComponent,
        ...Event_Card_Components,
        AddEventComponent,
        ...Event_Form_Components,
        EventTypeNamePipe
    ],
    exports: [
        EventsRegistryComponent,
        EventDisplayComponent
    ],
    providers: [AddEventService],
    entryComponents: [
      ...Event_Full_Components,
      ...Event_Card_Components,
      ...Event_Form_Components,
    ]
})
export class EventsModule { }
