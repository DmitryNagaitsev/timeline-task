import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FinancialTransaction } from './../models/financial-transaction';
import { News } from './../models/news';
import { Event } from '../models/event';
import { AddEventService } from '../services/add-event.service';
import { EventStorageService } from '../services/event-storage.service';

@Component({
    templateUrl: './events-registry.component.html',
    styleUrls: ['./events-registry.component.scss']
})
export class EventsRegistryComponent implements OnInit {
  public timelineEvents: Event[] = [];

  public constructor(
    private readonly addEventService: AddEventService,
    private readonly router: Router,
    private readonly eventStorageService: EventStorageService
  ) { }

  public ngOnInit(): void {
    this.timelineEvents = this.eventStorageService.getEvents();
  }

  public addEvent(): void {
    this.router.navigate(['/events/create']);
  }
}
