import { Component } from '@angular/core';
import { Event } from '../../../../models/event';
import { FinancialTransaction } from '../../../../models/financial-transaction';

@Component({
  templateUrl: './transaction-card.component.html',
  styleUrls: ['./transaction-card.component.scss']
})
export class TransactionCardComponent {
  public event: FinancialTransaction;
}
