import { NewsCardComponent } from './news/news-card.component';
import { TransactionCardComponent } from './transaction/transaction-card.component';
import { EventTypes } from './../../../models/event-types';
import { Type } from '@angular/core';

export function resolveComponentType(type: EventTypes): Type<any> {
  switch (type) {
    case EventTypes.FinancialTransaction:
      return TransactionCardComponent;
    case EventTypes.News:
      return NewsCardComponent;
    default:
      throw new Error(`Компонент для типа ${type} не реализован`);
  }
}
