import {
  Component,
  Input,
  Type,
  ComponentFactoryResolver,
  ViewChild,
  ViewContainerRef,
  OnInit,
  ComponentRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Event } from '../../../models/event';
import { resolveComponentType } from './event-card.factory';

@Component({
  selector: 'event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit {
  public eventContentComponent: Type<any>;
  private componentRef: ComponentRef<any>;

  @Input() event: Event;
  @ViewChild('container', { read: ViewContainerRef }) _vcr;

  public constructor(
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly router: Router
  ) { }

  public ngOnInit(): void {
    // TODO: переделать на модалки
    const eventContentComponent = resolveComponentType(this.event.type);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(eventContentComponent);
    this.componentRef = this._vcr.createComponent(componentFactory);
    this.componentRef.instance.event = this.event;
  }

  public openFullInfo(): void {
    this.router.navigate(['/events/display', this.event.id]);
  }
}
