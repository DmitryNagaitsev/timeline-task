import { Component } from '@angular/core';
import { News } from './../../../../models/news';

@Component({
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent {
  public event: News;
}
