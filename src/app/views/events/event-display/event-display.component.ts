import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventStorageService } from './../services/event-storage.service';
import { resolveFullComponentType } from './event-display.factory';

@Component({
    templateUrl: './event-display.component.html',
    styleUrls: ['./event-display.component.scss']
})
export class EventDisplayComponent implements OnInit {
  @ViewChild('container', { read: ViewContainerRef }) _vcr;

  constructor(
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly eventStorage: EventStorageService
  ) { }

  public ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = +params['id'];
      const event = this.eventStorage.getById(id);
      const eventContentComponent = resolveFullComponentType(event.type);
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(eventContentComponent);
      const componentRef = this._vcr.createComponent(componentFactory);
      componentRef.instance.event = event;
    });
  }

  public navigateToRegistry(): void {
    this.router.navigate(['/events/registry']);
  }
}
