import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EventStorageService } from './../../../../services/event-storage.service';
import { Event } from '../../../../models/event';
import { FinancialTransaction } from '../../../../models/financial-transaction';

@Component({
  templateUrl: './transaction-full.component.html',
  styleUrls: ['./transaction-full.component.scss']
})
export class TransactionFullComponent {
  public event: FinancialTransaction;

  public constructor(
    private readonly eventStorage: EventStorageService,
    private readonly router: Router
  ) { }

  public deleteEvent(): void {
    this.eventStorage.removeEvent(this.event.id);
    this.router.navigate(['/events/registry']);
  }
}
