import { Component, OnInit } from '@angular/core';
import { EventStorageService } from './../../../../services/event-storage.service';
import { News } from './../../../../models/news';

@Component({
  templateUrl: './news-full.component.html',
  styleUrls: ['./news-full.component.scss']
})
export class NewsFullComponent implements OnInit {
  public event: News;

  public constructor(
    private readonly eventStorage: EventStorageService
  ) { }

  public ngOnInit(): void {
    this.eventStorage.markViewed(this.event.id);
  }
}
