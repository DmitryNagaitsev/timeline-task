import { Type } from '@angular/core';
import { EventTypes } from '../models/event-types';
import { TransactionFullComponent, NewsFullComponent } from './components/cards';

export function resolveFullComponentType(type: EventTypes): Type<any> {
  switch (type) {
    case EventTypes.FinancialTransaction:
      return TransactionFullComponent;
    case EventTypes.News:
      return NewsFullComponent;
    default:
      throw new Error(`Компонент для типа ${type} не реализован`);
  }
}
