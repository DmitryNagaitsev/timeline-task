export enum EventTypes {
  /**
   * Финансовая транзакция
   */
  FinancialTransaction = 1,

  /**
   * Новость
   */
  News = 2
}
