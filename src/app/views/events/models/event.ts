import { EventTypes } from './event-types';

export abstract class Event {
  public id: number;
  public date: Date;

  public abstract get type(): EventTypes;
}
