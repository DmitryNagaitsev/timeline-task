import { Event } from './event';
import { EventTypes } from './event-types';

export class FinancialTransaction extends Event {
  public total: number;
  public currency: string;
  public transactionFrom: string;
  public description: string;

  public get type(): EventTypes {
    return EventTypes.FinancialTransaction;
  }

  public isConsumption(): boolean {
    return this.total >= 0;
  }
}
